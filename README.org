#+title: fourash
paja's imageboard
* Installation
  #+BEGIN_SRC sh
   $ shards install
   $ crystal run src/fourash.cr
  #+END_SRC
* TODO Usage
Write usage instructions here

* TODO Development
Write development instructions here

* Contributing

1. [[https://gitlab.com/ashandme/4ash/-/forks/new][Fork it!]]
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

* Contributors

- [[https://gitlab.com/ashandme][ashandme]] - creator and maintainer
- [[https://gitlab.com/martnpz][Martín]] - Jeffrey Epstein
