require "kemal"

get "/" do
    posts = File.open("db.txt", "r")
    unless posts
        File.new("db.txt", "w")
    end
    render "src/views/index.ecr"
end

get "/post" do
    render "src/views/create.ecr"
end

post "/post" do |env|
    env.redirect "/post"
end

post "/create" do |env|
    title = env.params.body["title"].as(String)
    caption = env.params.body["caption"].as(String)
    File.open("db.txt", "w") do |f|
        f.puts "**#{title}**#{caption}"
    end
    env.redirect "/"
end

get "/404" do |env|
    error 404 do
        "Algo turbio pasó."
    end
end

Kemal.config.port = 1488
Kemal.run
